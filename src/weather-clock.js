var weather = require('weather-js')
var $ = require('jQuery')

module.exports = function () {
  let weatherClock = {
    refresh: function () {
      getWeather()
      getTime()
    }
  }

  function getWeather () {
    weather.find({
      search: 'Toronto, CA',
      degreeType: 'F'
    }, function (err, result) {
      if (err) console.log(err)
      // var res = JSON.stringify(result, null, 2)
      $('#weather .temp').text(result[0]['current']['temperature'])
      $('#weather .type').text(result[0]['current']['skytext'])
      $('#weather .temp_pic img').attr('src', result[0]['current']['imageUrl'])
    })
  }

  function getTime () {
    var time = new Date()
    $('#time span').html(time.toLocaleString('en-US', {
      hour: 'numeric', minute: 'numeric', hour12: true
    }))

    var monthNames = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ]

    var date = new Date()
    var day = date.getDate()
    var month = monthNames[date.getMonth()]
    var year = date.getFullYear()
    var thisDay = month + ' ' + day + ', ' + year
    $('#date').html(thisDay)
  }

  weatherClock.refresh()
  return weatherClock
}
