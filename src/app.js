'use strict'
const WeatherClock = require('./weather-clock')
const BluetoothReader = require('./bluetooth-reader')
const Schedule = require('./schedule')
const PhotoStream = require('./photo-stream')
let $ = require('jQuery')

let config = {
  //LIVE
  apiBaseUrl: 'https://api2.eventfinity.co/api/v1/admin/events/',
  //Staging
  //apiBaseUrl: 'https://api-stage.eventfinity.co/api/v1/admin/events/',
  
  //LIVE
  eventId: 51,
  //Staging
  //eventId: 27,

  admin: {  
    //LIVE
    api_auth_token: 'efadb0b84986c0a57bb07a191c5c9a2aaa2496bfe530cbd70de333bc0135d9c0'
    //Staging
    //api_auth_token: 'da83ffbfec2403e9a835b1224973739e52bc90f34f5e6bb5721e42d8e1096fa8'
  },
  
  //LIVE
  photoStreamId: 131,
  //Staging
  //photoStreamId: 13,
  
  //LIVE
  signAttendeeId: 140367,
  //Staging
  //signAttendeeId: 25318,

  attendeeAdditionalInfoFieldKeys: ["mc", "breakouts", "casey"],
  
  footerMessage: 'Make sure your phone\'s Bluetooth is turned on. Open your UPS meeting app, ' +
    'hit "Connect Account" and tap your mobile device on the bluetooth reader ' +
    'below the screen to access your personalized agenda.',
  browseText: 'Browse the Event Agenda or tap your mobile device to the reader below for your personalized info.'
}

$(document).ready(function () {
  let weatherClock = WeatherClock()
  setInterval(function () {
    weatherClock.refresh()
  }, 60000)

  let reader = BluetoothReader(config)
  reader.start()

  let schedule = Schedule(config)
  schedule.fetchSchedule(config.signAttendeeId, function (sched) {
    console.log("sched")
    console.log(sched)
    $('.bluetooth-notice').text(config.footerMessage)
  }, function (err) {
    console.log(err)
  })

  let photoStream = PhotoStream(config)
  photoStream.fetchPhotos(config.signAttendeeId, function (stream) {
    console.log(stream)
  }, function (err) {
    console.log(err)
  })

  window.setInterval(function(){
    let schedule = Schedule(config)
    schedule.fetchSchedule(config.signAttendeeId, function (sched) {
      console.log(sched)
      $('.bluetooth-notice').text(config.footerMessage)
    }, function (err) {
      console.log(err)
    })

    photoStream.fetchPhotos(config.signAttendeeId, function (stream) {
      console.log(stream)
    }, function (err) {
      console.log(err)
    })
  }, 5 * 60 * 1000);
})
