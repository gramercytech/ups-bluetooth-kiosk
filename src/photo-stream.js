const $ = require('jQuery')

module.exports = function (config) {
  var stream = {
    photos: [],
    attendeeId: undefined,

    insertPhotos: function () {
      $('#your-photos').hide()
      $('.single-photo').not('.hidden').remove()
      stream.photos.forEach(function (photo) {
        if (photo.photo_url_medium) addThumb(photo)
      })
      moveUpPhotosForThisAttendee(this.attendeeId)
    },

    fetchPhotos: function (attendeeId, onSuccess, onError) {
      let self = this
      $.ajax({
        type: 'GET',
        url: config.apiBaseUrl + config.eventId + '/photo-streams/' + config.photoStreamId + '/photos?per-page=500',
        data: config.admin,
        dataType: 'json',
        success: function (response) {
          self.photos = response.data
          self.attendeeId = attendeeId
          self.insertPhotos()
          onSuccess(self)
        },
        error: function (response) {
          console.log('something went wrong:', response)
          onError(response)
        }
      })
    }
  }

  function addThumb (photo) {
    let clone = $('.single-photo.hidden').clone()
    clone.removeClass('hidden')
    clone.data('attendee', photo.event_attendee_id)
    
    let taggedAttendeeIds = ""
    photo.tagged_attendee_names.forEach(function (person) {
      taggedAttendeeIds += person.event_attendee_id+ " "
    })

    clone.data('taggedAttendeeIds', taggedAttendeeIds)
    clone.find('img').attr('src', photo.photo_url_medium)
    clone.click(function(){
      $('.modal').find('.maps').hide()
      $('.modal').find('.photo').show()
      $('.modal').find('.photo img').first().attr('src', photo.photo_url)
      $('.modal').find('.schedule-item-title').text("Photo by "+photo.attendee_name)
      $('.modal').find('.schedule-item-details').show()
      $('.modal').find('.schedule-item-time').hide()  
      $('.modal').find('.schedule-item-location').html(photo.photo_caption)
      
      if (photo.tagged_attendee_names.length > 0) {
        let taggedString = ""
        photo.tagged_attendee_names.forEach(function (person, i) {
          if (i+1 == photo.tagged_attendee_names.length) {
            taggedString += person.first_name+" "+person.last_name
          } else {
            taggedString += person.first_name+" "+person.last_name+ ", "
          }
        })
        $('.modal').find('.photo-item-tags').show()
        $('.modal').find('.photo-item-tags').html("Tagged: "+taggedString)
      } else {
        $('.modal').find('.photo-item-tags').html("")
      }
      $('.modal').find('.schedule-item-description').hide()
      $('.modal').css('display', 'flex')
    })
    $('#stream').append(clone)
  }

  function moveUpPhotosForThisAttendee (attendeeId) {
    $('#stream .single-photo').not('.hidden').each(function () {
      if ($(this).data('attendee') === attendeeId || $(this).data('taggedAttendeeIds').includes(attendeeId)) {
        $('#your-photos').show()
        $(this).appendTo($('#your-stream'))
      }
    })
  }

  return stream
}
