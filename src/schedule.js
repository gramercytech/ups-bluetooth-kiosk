const moment = require('moment')
const $ = require('jQuery')

const Analytics = require('electron-google-analytics');
const analytics = new Analytics.default('UA-70440260-2');

module.exports = function (config) {
  var schedule = {
    eventAttendeeId: undefined,
    days: [],
    currentDay: undefined,
    attendeeAdditionalInfo: undefined,

    getFormattedDates: function () {
      this.days.forEach(function (day) {
        day.formatted_date = moment(day.scheduled_date).format('dddd, MMMM D')
      })
    },

    insertScheduleItems: function (day) {
      let self = this
      $('.agenda-item').not('.hidden').remove()
      day.items.forEach(function (item, index) {
        self.insertScheduleItem(item, index)
      })
      setScheduleItemListeners()
    },

    insertScheduleItem: function (item, index) {
      var clone = $('.agenda-item.hidden').clone()
      clone.removeClass('hidden')
      clone.data('item', index)
      this.insertScheduleItemFields(item, clone)
      $('#agenda').append(clone)
    },

    insertScheduleItemFields: function (item, row) {
      row.find('.agenda-time').text(getTimeString(item))
      let icon = row.find('.agenda-logo > i')
      icon.addClass('fa ' + getIcon(item.icon_class))
      if (!item.icon_color === '#fff') icon.css('color', item.icon_color)
      row.find('.agenda-name').text(item.name)
    },

    setDay: function (index) {
      if (index < this.days.length && index > -1) {
        this.currentDay = index
        $('.agenda-date').text(this.days[this.currentDay].formatted_date)
        hideNonfunctioningArrows(index, this.days.length)
        this.insertScheduleItems(this.days[this.currentDay])
      }
    },

    nextDay: function () {
      this.setDay(this.currentDay + 1)
    },

    previousDay: function () {
      this.setDay(this.currentDay - 1)
    },

    fetchSchedule: function (attendeeId, onSuccess, onError) {
      $.ajax({
        type: 'GET',
        url: config.apiBaseUrl + config.eventId + '/eventAttendees/' + attendeeId,
        data: config.admin,
        dataType: 'json',
        success: function (response) {
          initializeScheduleFromResponse(response, attendeeId)
          onSuccess(schedule)
        },
        error: function (response) {
          console.log('something went wrong:', response)
          onError(response)
        }
      })
    }
  }

  function initializeScheduleFromResponse (response, attendeeId) {
    let name = response.data.attendee.first_name + ' ' + response.data.attendee.last_name
    updateWelcomeText(name)
    updateFooterMessage(name)
    let allItems = response.data.scheduleItems.sort(sortByStartTime)
    schedule.days = sortScheduleItemsIntoDays(allItems)
    schedule.eventAttendeeId = attendeeId
    schedule.getFormattedDates()
    attendeeAdditionalInfo = response.data.attendee.additional_info

    var d = new Date();
    //hackey af
    if (d.getDay() == 1) {
      //monday
      schedule.setDay(0)
    } else if (d.getDay() == 2) {
      //tuesday
      schedule.setDay(1)
    } else if (d.getDay() == 3) {
      //wednesday
      schedule.setDay(2)
    } else if (d.getDay() == 4) {
      //thursday
      schedule.setDay(3)
    } else {
      //dont care
      schedule.setDay(0)
    }
    
    setDateNavListeners()
    sendAnalytics(attendeeId, response.data.attendee.email)
  }

  function sendAnalytics(attendeeId, attendeeEmail) {
    if (attendeeId != config.signAttendeeId) {
      // Google Analytics
      analytics.event('attendee', attendeeEmail, { evLabel:Date(), evValue: attendeeId})
        .then((response) => {
          return response;
        }).catch((err) => {
          return err;
        });
    }
  }

  function sortByStartTime (a, b) {
    let aTime = new Date(a.scheduled_date + ' ' + a.start_time)
    let bTime = new Date(b.scheduled_date + ' ' + b.start_time)
    if (aTime < bTime) return -1
    if (aTime > bTime) return 1
    return 0
  }

  function updateWelcomeText (name) {
    let inUse = name !== 'Digital Signage'
    let welcomeText = inUse ? 'Hello, ' + name + '!' : 'Welcome!'
    let browseText = inUse ? 'Browse your personal agenda' : config.browseText
    $('#welcome-text').text(welcomeText)
    $('#browse-text').text(browseText)
  }

  function updateFooterMessage (name) {
    $('.bluetooth-notice').text(`You are now viewing the agenda and photos as ${name}.`)
  }

  function getDay (days, item) {
    return days.reduce(function (accum, day) {
      if (accum) return accum
      if (day.scheduled_date === item.scheduled_date) return day
    }, undefined)
  }

  function sortScheduleItemsIntoDays (items) {
    return items.reduce(function (days, item) {
      var day = getDay(days, item)
      if (day) day.items.push(item)
      else days.push({scheduled_date: item.scheduled_date, items: [item]})
      return days
    }, [])
  }

  function setDateNavListeners () {
    $('#date-nav-arrow-left').click(function () {
      schedule.previousDay()
    })
    $('#date-nav-arrow-right').click(function () {
      schedule.nextDay()
    })
  }

  function setScheduleItemListeners () {
    $('.agenda-item.row').click(function () {
      var index = $(this).data('item')
      var item = schedule.days[schedule.currentDay].items[index]
      $('.modal').find('.maps').hide()
      $('.modal').find('.photo').hide()
      $('.modal').find('.photo-item-tags').hide()
      $('.modal').find('.schedule-item-details').show()
      $('.modal').find('.schedule-item-time').show()
      $('.modal').find('.schedule-item-title').text(item.name)
      $('.modal').find('.schedule-item-time').text(getTimeString(item))
      if (getLocation(item.locations) != "" && getLocation(item.locations) != "zzz") {
        $('.modal').find('.schedule-item-location').html("<i class=\"fa fa-map-marker-alt\"></i> "+getLocation(item.locations))
      } else {
        $('.modal').find('.schedule-item-location').html("")
      }
      $('.modal').find('.schedule-item-description').show()
      if (item.description) {
        $('.modal').find('.schedule-item-description').html(getDescription(item.description))
      } else {
        $('.modal').find('.schedule-item-description').html("")
      }
      $('.modal').css('display', 'flex')
    })
  }

  //again, hackey af. description should be formatted server side.
  function getDescription (unformatted) {
    var json = JSON.parse(attendeeAdditionalInfo)
    if(json) {
      var formatted = unformatted
      for (key in json ) {
        var regex = new RegExp("\\["+key+"\\]", "g")
        formatted = formatted.replace(regex, json[key])
      }
      return formatted
    } else {
      var formatted = unformatted
      for(var i = 0; i < config.attendeeAdditionalInfoFieldKeys.length; i++) {
        var regex = new RegExp("\\["+config.attendeeAdditionalInfoFieldKeys[i]+"\\]", "g")
        formatted = formatted.replace(regex, "")
      }
      return formatted
    }
  }

  function hideNonfunctioningArrows (index, dayCount) {
    $('.date-nav-arrow').css('opacity', 1)
    if (index < 1) $('#date-nav-arrow-left').css('opacity', 0)
    if (index >= dayCount - 1) {
      $('#date-nav-arrow-right').css('opacity', 0)
    }
  }

  function getIcon (iconClass) {
    let alternates = {
      'fa-cutlery': 'fa-utensils', 'fa-video-camera': 'fa-video', 'fa-glass': 'fa-glass-martini'
    }
    return alternates[iconClass] ? alternates[iconClass] : iconClass
  }

  function getTimeString (scheduleItem) {
    let start = moment('2018-01-01 ' + scheduleItem.start_time).format('h:mma')
    let end = moment('2018-01-01 ' + scheduleItem.end_time).format('h:mma')
    return start + ' - ' + end
  }

  function getLocation (locations) {
    return locations.length > 0 ? locations[0].name : ''
  }

  return schedule
}
