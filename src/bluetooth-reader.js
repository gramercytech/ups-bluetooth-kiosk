const WebSocket = require('ws')
const Schedule = require('./schedule')
const PhotoStream = require('./photo-stream')
const $ = require('jQuery')

module.exports = function (config) {
  let secondsToListen = 8
  let maxSecondsIdleWhileInUse = 60
  let UPSer = '0000125027'

  let connection = {
    listening: false,
    timeout: undefined,
    attendeeId: config.signAttendeeId,

    listen: function () {
      this.listening = true
      console.log('listening')
      this.limitListeningTime()
    },

    limitListeningTime: function () {
      let self = this
      if (this.timeout) clearTimeout(this.timeout)
      this.timeout = setTimeout(function () {
        if (self.listening) self.onTimeout()
      }, secondsToListen * 1000)
    },

    limitIdleTime: function () {
      let self = this
      if (this.timeout) clearTimeout(this.timeout)
      this.timeout = setTimeout(function () {
        if(self.attendeeId !== config.signAttendeeId) {
          self.dropConnection()
        }
      }, maxSecondsIdleWhileInUse * 1000)
    },

    onTimeout: function () {
      this.listening = false
      $('.loading').text('No signal was found. Try again')
      let self = this
      this.timeout = setTimeout(function () {
        self.cancel()
      }, 2000)
    },

    cancel: function () {
      this.listening = false
      toggleConnectionLoader()
      toggleBluetoothMessageAndCancel()
      $('.bluetooth-notice').text(config.footerMessage)
      $('.loading').text('Searching for signal')
    },

    handleDataFromWebsocket: function (data) {
      if (this.listening) {
        this.listening = false
        let splitData = data.split('-')
        let attendeeId = Number(splitData[splitData.length - 1])
        this.replaceSchedule(attendeeId)
        this.updatePhotos(attendeeId)
      }
    },

    updatePhotos: function (attendeeId) {
      let photoStream = PhotoStream(config)
      photoStream.fetchPhotos(attendeeId, function (stream) {
        console.log(stream)
      }, function (err) {
        console.log(err)
      })
    },

    replaceSchedule: function (attendeeId) {
      let schedule = Schedule(config)
      let self = this
      schedule.fetchSchedule(attendeeId, function (success) {
        console.log(success)
        self.attendeeId = attendeeId

        if (attendeeId === config.signAttendeeId) $('.bluetooth-notice').text(config.footerMessage)
        else {
          toggleConnectDisconnect()
          toggleConnectionLoader()
          toggleBluetoothMessageAndCancel()
          self.limitIdleTime()
        }
      }, function (err) {
        console.log(err)
        self.onTimeout()
      })
    },

    dropConnection: function () {
      this.replaceSchedule(config.signAttendeeId)
      this.updatePhotos(config.signAttendeeId)
      toggleConnectDisconnect()
    },

    debugMessage: function (message) {
      this.handleDataFromWebsocket(message)
    },

    start: function () {
      initWSServer(this)

      let self = this
      $(document).keydown(function (event) {
        // this is for dev purposes only
        if (event.key === 'a') self.debugMessage('abcdef-ghijk-lmno-' + UPSer)
      })

      $('.connect-button').click(function () {
        self.listen()
        toggleBluetoothMessageAndCancel()
        toggleConnectionLoader()
/*      //Debug connection
        if (self.listening) {
          self.listening = false
          let attendeeId = 124917
          self.replaceSchedule(attendeeId)
          self.updatePhotos(attendeeId)
        }
*/
      })

      $('.disconnect-button').click(function () {
        self.dropConnection()
      })

      $('.cancel-button').click(function () {
        self.cancel()
      })

      $('body').on('touchstart', function () {
        if (self.attendeeId !== config.signAttendeeId) {
          self.limitIdleTime()
        }
      })
    }
  }

  function initWSServer (reader) {
    const wss = new WebSocket.Server({ port: 8080 })
    wss.on('connection', function connection (ws) {
      ws.on('message', function incoming (message) {
        console.log(message)
        reader.handleDataFromWebsocket(message)
      })

      ws.send('something')
    })
  }

  function toggleConnectionLoader () {
    $('.loading').toggleClass('hidden')
    $('#connect').toggleClass('hidden')
  }

  function toggleBluetoothMessageAndCancel () {
    $('.cancel-button').toggleClass('hidden')
    //$('.bluetooth-notice').toggleClass('hidden')
  }

  function toggleConnectDisconnect () {
    $('.connect-button').toggleClass('hidden')
    $('.disconnect-button').toggleClass('hidden')
    $('.animated-arrows').toggleClass('hidden')
  }

  return connection
}
